#KVIZ
##by Dario Đurić

**upute**
Prilikom ulaska u kviz, unesite svoje ime po kojem će se bilježiti vaš rezultat.

Nakon poruke dobrodošlice, kviz generira 10 pitanja iz datoteke pitanja.txt.

Svako pitanje ima 3 ponuđena odgovora, te je samo 1 točan.

Na pitanja se odgovara sa a, b ili c, a kviz nije case sensitive.

Nakon što odgovorite na svih 10 pitanja, dobit ćete poruku u kojoj će biti napisan vaš rezultat.

Na kraju se nalazi Hall of Fame: iz datoteke results.txt učitava se 10 najboljih, sortiranih po rezultatu, od najvećeg broja bodova, prema najmanjem.


**dodavanje pitanja**

Pitanja se nalaze u datoteci pitanja.txt

Zapisana su u JSON formatu na način:

    pitanjeBROJPITANJA : tekst pitanja

    odgovor_aBROJPITANJA : tekst odgovora a

    odgovor_bBROJPITANJA : tekst odgovora b

    odgovor_cBROJPITANJA : tekst odgovora c

    točan_odgovorBROJPITANJA : slovo točnog odgovora(bez broja pitanja)

U datoteci help.txt nalazi se pripremljeno još pitanja, kojima fali tekst pitanja i odgovori, te naravno, točan odgovor

Ukoliko se doda još pitanja u datoteku pitanja.txt, rad će biti neometan, nije hardkodiran broj pitanja

**nakon zadnjeg pitanja u datoteci pitanja.txt mora se obrisati zarez -> na kraju reda ostaje zatvoreni nazivnik, te u idućem redu zatvorena vitičasta zagrada**


**rezultati**

Rezultati se nalaze u datoteci results.txt.

Zapisani su u CSV formatu na način:

    ime, ostvareni rezultat
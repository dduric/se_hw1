"""
Kviz
by Dario Djuric
"""
import json
import random
import csv

print("Unesite vase ime")
name = raw_input()
print("Dobrodosli u kviz " + name + "\n")
json_questions = open("/home/dario/Desktop/kviz/pitanja.txt", "r")
num = json_questions.readlines()[-2][8:10]
json_questions.close()
json_questions = open("/home/dario/Desktop/kviz/pitanja.txt", "r")
question = json.load(json_questions)
json_questions.close()

question_numbers = []
while len(question_numbers) < 10:
    n = random.randint(1,int(num))
    for i in range(len(question_numbers)):
        while n == question_numbers[i]:
            n = random.randint(1,int(num))
    question_numbers.append(n)

print (question_numbers)
score = 0

for i in range(10):
    x = question_numbers[i]
    getQuestion = "question" + str(x)
    getAnswer = "correct" + str(x)
    getA = "a" + str(x)
    getB = "b" + str(x)
    getC = "c" + str(x)

    print(question[getQuestion])
    print("a) " + question[getA])
    print("b) " + question[getB])
    print("c) " + question[getC])

    answer = raw_input()
    
    if answer.lower() == question[getAnswer]:
        score+=1

print "Cestitam " + name + "! Tocno ste odgovorili na " + str(score) + " od 10 pitanja!"

result_path = "/home/dario/Desktop/kviz/results.txt"
csv_results = open(result_path, "a")
writer = csv.writer(csv_results)
writer.writerow([name, score])

print "Hall of fame"
csv_hof = open(result_path)
reader = csv.reader(csv_hof, delimiter=',')
header = next(reader)
results = []
for row in reader:
    ime = str(row[0])
    rez = int(row[1])
    results.append([ime, rez])

halloffame = sorted(results, key=lambda x:int(x[1]), reverse=True)
for row in halloffame[:10]:
    print "Name: " + str(row[0]) + " Score: " + str(row[1])